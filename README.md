# OMZ Gentoo Modified

The customized theme I use for Oh My Zsh (i.e. my ZSH prompt).

## Installation

Requirements:

- [Z Shell](https://www.zsh.org/)
- [Oh My Zsh](https://ohmyz.sh/)

Once you have these installed, simply run the `install.zsh` script.

## Acknowledgements

This theme is a very small variation on the `gentoo` theme that comes with Oh My
Zsh. You can check it out [on
GitHub](https://github.com/ohmyzsh/ohmyzsh/blob/master/themes/gentoo.zsh-theme).

## License & Copyright

My modifications are absolutely minor, and in keeping with the original license
intention of the author I will also be distributing my theme under the [MIT
license](/LICENSE).
