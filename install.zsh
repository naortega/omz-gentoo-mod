#!/usr/bin/env zsh

OMZ_HOME=$HOME/.oh-my-zsh

if ! [ -d $OMZ_HOME ]
then
	echo "Failed to install. Oh My Zsh is not installed (in $OMZ_HOME)."
	echo "Please install Oh My Zsh first."
	exit 1
fi

install -m 644 ./omz-gentoo-mod.zsh-theme $OMZ_HOME/custom/themes
